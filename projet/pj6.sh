#!/bin/bash
#saisir fichier
#verify fichier
FILE=""
GROUP=""
USER=""
#user=U Fichier=F Group=G

saisir_FILE()
{
	FILE=""
	while [ -z "$FILE" ]
	do
		echo "Saisir le nom de fichier."
		read FILE
	done
	verify_FILE
}
verify_FILE()
{
	if [ -f "$FILE" ]
	then
		echo "Le fichier $FILE existe."
		saisir_ACTION
	else
		echo "Le fichier $FILE n'existe pas."
		FILE=""
		saisir_FILE
	fi
}

saisir_USER()
{
	USER=""
	while [ -z "$USER" ]
	do 
		echo " Saisir le nom du nouvel user que vous voulez changer."
		read USER
	done
	verify_USER	
}
verify_USER()
{
	if grep "^$USER:" /etc/passwd > /dev/null
	then
		echo "User $USER existe."
	else
		echo "User $USER n'existe pas."
		sudo useradd $USER
		echo "Nouvel user $USER a bien ajouté."
	fi
	sudo chown $USER $FILE
	echo " $FILE a changé utilisateur: $USER."
	saisir_ACTION
}
saisir_GROUP()
{
	GROUP=""
	while [ -z "$GROUP" ]
	do
		echo "Saisir le nom du nouvel group que vous voulez changer."
		read GROUP
	done
	verify_GROUP
	}
verify_GROUP()
{
	if grep "^$GROUP:" /etc/group > /dev/null 
	then
		echo "Le group $GROUP existe."
	else
		echo "LE group $GROUP n'existe pas."
		sudo groupadd $GROUP
		echo "Nouvel group $GROUP a bien ajouté."
	fi
	sudo chgrp $GROUP $FILE
	echo " $FILE a changé de group : $GROUP."
	saisir_ACTION
}
saisir_ACTION()
{
REP=0
while [ "$REP" = 0 ]
do
	read -p "Vous voulez [chown/chgrp/quit] " ACTION	
	case $ACTION in
		"chown")
			saisir_USER
			;;
		"chgrp")	
			saisir_GROUP			
			;;
		"quit")
			exit
			;;
		*)	
			echo "error, command $ACTION inconnue"
			;;
	esac
done
}
saisir_FILE

