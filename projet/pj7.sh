#!/bin/bash
#Saisir et verify fichier 
#saisir et verify droit
FILE=""
DROIT_DU_PROPRIETAIRE=""
GROUP="group"
OTHER="other"

saisir_file()
{
	FILE=""
	while [ -z "$FILE" ]
	do
		echo "Saisir le nom de fichier."
		read FILE
	done
	verify_file
}
verify_file()
{
	if [ -f "$FILE" ]
	then
		echo "Le fichier $FILE existe."
		saisir_DROIT_DU_PROPRIETAIRE

	else
		echo "Le fichier $FILE n'existe pas."
		FILE=""
		saisir_file
	fi
}
saisir_DROIT_DU_PROPRIETAIRE()
{
	DROIT_DU_PROPRIETAIRE=""
	while [ -z "$DROIT_DU_PROPRIETAIRE" ]
	do 
		echo " Saisir le nouvel droits pour le fichier:(r-read; w-write; x-excution)"
		read DROIT_DU_PROPRIETAIRE
	done
	verify_DROIT_DU_PROPRIETAIRE
}
verify_DROIT_DU_PROPRIETAIRE()
{
	if [[ "$DROIT_DU_PROPRIETAIRE" == "rwx" || "$DROIT_DU_PROPRIETAIRE" == "rw" || "$DROIT_DU_PROPRIETAIRE" == "rx" || "$DROIT_DU_PROPRIETAIRE" == "r" || "$DROIT_DU_PROPRIETAIRE" == "wx" || "$DROIT_DU_PROPRIETAIRE" == "wr" || "$DROIT_DU_PROPRIETAIRE" == "w" || "$DROIT_DU_PROPRIETAIRE" == "x" ]]
	then
		echo " vous voulez changer: $DROIT_DU_PROPRIETAIRE."
		saisir_action
	else
		echo " DROIT_DU_PROPRIETAIRE n'est pas correct."
		saisir_DROIT_DU_PROPRIETAIRE
	fi
}
saisir_action()
{
ACTION=""
REP=0
while [ "$REP" = 0 ]
do	
	echo "Vous voulez changer le droit vers [usr/grp/other/quit] :"
	read ACTION
	case $ACTION in 
		usr)
			echo " Saisir le nom du user que vous voulez le mettre du nouvel droit pour le fichier $FILE."
			read USER
			USER=""
			if [ grep "^$USER:" /etc/passwd > /dev/null ] && [ -z "$USER" ]
			then
				sudo chmod u=$DROIT_DU_PROPRIETAIRE $FILE 
				echo " le droit du $DROIT_DU_PROPRIETAIRE proprietaire a changé pour le fichier $FICHIER vers le nouvel user $USER ."
			else
				echo " $USER n'existe pas ."
				exit	 
			fi
			;;
		grp)	
			echo " Saisir le nom du group que vous voulez le mettre du nouvel droit pour le fichier $FILE."
       			read GROUP
			GROUP=""
			if [ grep "^$GROUP:" /etc/group > /dev/null ] && [ -z "$GROUP" ]	
			then
				sudo chmod g=$DROIT_DU_PROPRIETAIRE $FILE
				echo " le droit du $DROIT_DU_PROPRIETAIRE proprietaire a changé pour le fichier $FICHIER vers le nouvel group $GROUP. " 
			else
				echo " $GROUP n'existe pas."
				exit	
			fi
			;;
		other)
			echo " Saisir les nom du others que vous voulez le mettre du nouvel droit pour le fichier $FILE."
		       	read OTHER	
			OTHER=""
			if [ grep "^$OTHER:" /etc/other > /dev/null ] && [ -z "$OTHER" ]
			then
				sudo chmod o=$DROIT_DU_PROPRIETAIRE $FILE
				echo " le droit du $DROIT_DU_PROPRIETAIRE proprietaire a changé pour le fichier $FICHIER ver l'others $OTHER."
			else
				echo " $OTHER n'existe pas."
				exit
			fi	
			;;
		quit)
			exit
			;;
		*)
			echo " le saisir n'a pas reconnu, au revoir."
			exit
			;;
	esac
done
}
saisir_file

